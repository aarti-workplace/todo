from flask import Flask, render_template, url_for, redirect
from flask_socketio import SocketIO,send,emit
import pymongo
from pymongo import MongoClient
from bson import json_util
from bson.json_util import dumps 
import json
from datetime import datetime

app = Flask(__name__)

client = MongoClient('mongodb://localhost:27017/')
db = client['flask']
# todos = db['todo']

socketio = SocketIO(app)
datetime = datetime.now()
#Function to autoincreament the other collection id
def getNextSequence(collection,name):  
         return collection.find_and_modify(query= { '_id': name },update= { '$inc': {'seq': 1}}, new=True ).get('seq')  

@app.route('/')
def index():
    # res = db.users.insert({'_uid': getNextSequence(db.counter,"userid"), 'name': "amruta"})  
    return render_template('socketpage.html')

@socketio.on('mytask')
def add(taskname):
    
    db.todos.insert_one({'_taskid': getNextSequence(db.todo_counter, "todoid"), 'taskname': taskname, 'complete':False,'inserted_date':datetime.strftime("%m/%d/%Y %I:%M%p")})
    #Method to convert pymongo cursor to json
    cursor = db.todos.find()
    list_cur = list(cursor)
    # get a cursor length 
    length = len(list_cur)
    json_data = dumps(list_cur)
    
    
    # print(myvalues)
    #emit this json data to javascript event to handle all data
    emit('mytaskresponse',{'data': json_data, 'length':length}) 


if __name__ == '__main__':
    socketio.run(app, debug = True)